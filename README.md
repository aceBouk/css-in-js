
# Why css-in-js ?  


first let's talk about the **problem** that css-in-js try to solve.  
The bigger your application gets (and the more CSS you write), the more globals you will create which is quite simply making your application harder to maintain and use. In order to work around these scaling issues, naming strategies like Atomic CSS, OOCSS, SMACSS, SUITCSS, Object-oriented CSS or tools like LESS, SASS, etc are often utilized.  


CSS in JS solves this problems cleanly, clearly, and simply.  


# Impact of css-in-js  


### Eliminating globals :  

yes everything in CSS is a global:  


Luckily, there is an outstanding and mature solution to eliminating globals available: [CSS modules](https://github.com/css-modules/css-modules). This comes bundled with Webpack’s CSS loader, so you can already use it, without doing anything.  


but if you want to avoid webpack *css-in-js* is a good option  


### Style dependencies :  

CSS-in-JS makes it simple to include styles with components. And this is actually a huge benefit. Nobody likes maintaining entirely separate file trees just for their CSS, or managing CSS files which live in completely separate locations to their corresponding JSX.  


But now that we have tools like Webpack and create-react-app, this benefit isn’t limited to CSS-in-JS. In fact, I place my SCSS files right next to my JSX and then include them like this:  

import './style.scss'  


#### Composition of styles  


## general benifits:  

CSS-in-JS does have a great part: **it isn’t CSS** 😜.  


**You can’t use CSS in react-native apps.** But you _can_ use CSS-in-JS with [styled-components](https://www.styled-components.com/).  


- Live editing and near instant preview of the code changes (using a web worker and the Bable transpiler!)  
- Service worker integration for better offline support  
- A _rudimentary_ file system! Add a file, and import it in the main index file!  
- Persistence! Update any file (or add additional files) and you can share that URL with your friends  
- Theming by way of a light/dark theme  



## Conclusion:  

So far all my ranting about CSS-in-JS, I don’t think it is a terrible technology. In fact, I think it is actually pretty cool and I encourage to use it. It did was provide new solutions to old problems but like any new technology, it introduces its own problems too.
